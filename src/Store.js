import React, {useState} from "react";

export const UserLoggedInContext = React.createContext({ userLogged: 'false', setUserLogged: ()=>{} })
export const FormTypeContext = React.createContext({ formType: 'login', setFormType: ()=>{} })

const Store = ({children}) => {
    const [userLogged, setUserLogged] = useState('false');
    const [formType, setFormType] = useState('login');

    return (
        <UserLoggedInContext.Provider value={[userLogged, setUserLogged]}>
            <FormTypeContext.Provider value={[formType, setFormType]}>
                {children}
            </FormTypeContext.Provider>
        </UserLoggedInContext.Provider>
    )
}

export default Store;