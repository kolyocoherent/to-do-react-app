import React, {useContext, useState, useEffect} from "react";
import { UserLoggedInContext } from "../Store";
import Cookies from 'universal-cookie';
import axios, {setError} from "axios";

function LogedIn(){
    const [userLogged, setUserLogged] = useContext(UserLoggedInContext);
    const [showTasks, setShowTasks] = useState(true);
    const [taskLabel, setTaskLabel] = useState('');
    const [taskDescription, setTaskDescription] = useState('');
    const cookies = new Cookies();
    const addTaskBaseURL = "https://api-nodejs-todolist.herokuapp.com";
    const authenticateBaseURL = "https://api-nodejs-todolist.herokuapp.com/user/me";
console.log(userLogged)

    const axiosInstance = axios.create({
        addTaskBaseURL,
        headers: {'Authorization': `Bearer ${userLogged}`},
        data: {'test':'123'}
    })

    async function createPost() {
        let response = await axiosInstance.post('/task')
        if(response.status === 200 ){
            console.log(response.data)
        }
    }


    const signOut = () => {
        cookies.remove('loggedIn');
        setUserLogged("false")
    }

    const GoAddTask = () => {
        setShowTasks(false)
    }
    const ShowTasks = () => {}
    const AddTask = () => {
        //e.preventDefault();
        createPost()
    }
    return (
        <div>
            <div className="navbar">
                <button onClick={()=>GoAddTask()}>Add task</button>
                <button onClick={()=>ShowTasks()}>All tasks</button>
                <div className="search-bar">
                    <input></input>
                </div>
                <button onClick={() => signOut()}>Sign out</button>
            </div>

            {   
            showTasks ?
                <div className="tasks-list">
                    <p>all tasks</p>
                    {/* {tasks ? tasks : null} */}
                </div> 
            : 
                <div className="tasks-list">
                    <form>
                        <div className="form">
                            <h2>Add new task</h2>
                            <div className="form-group">
                                <label htmlFor="task-label">Task label</label>
                                <input id="task-label" name="task-label" type="text" onChange={(e) => setTaskLabel(e.target.value)}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="desctiption">Task description</label>
                                <input id="desctiption" name="desctiption" type="text" onChange={(e) => setTaskDescription(e.target.value)}></input>
                            </div>
                            {/* { error ? <p style={{color: "red"}}>{error}</p> : null } */}
                            <button onClick={()=>AddTask()}>Add to list</button>
                        </div>
                    </form>
                </div>
            }   
        </div>
    )
}

export default LogedIn