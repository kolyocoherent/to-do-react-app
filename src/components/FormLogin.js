import React, {useContext, useState} from "react";
import axios, {setError} from "axios";
import { UserLoggedInContext ,FormTypeContext } from "../Store";
import Cookies from 'universal-cookie';

function FormLogin(){
    const [userLogged, setUserLogged] = useContext(UserLoggedInContext);
    const [formType, setFormType] = useContext(FormTypeContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = React.useState(null);
    const [token, setToken] = React.useState(null);
    const baseURL = "https://api-nodejs-todolist.herokuapp.com/user/login";
    const authenticateBaseURL = "https://api-nodejs-todolist.herokuapp.com/user/me";
    const cookies = new Cookies();
    
    function createPost() {
        axios
        .post(baseURL, {
            email: email,
            password: password
        })
        .then((response) => {
            cookies.set('loggedIn', true, { path: '/' });
            setUserLogged(response.data.token)
        }).catch(error => {
            setError(error.response.data);
        });
    }
    const loginUser = e => {
        e.preventDefault();
        createPost()
    }
    return (
        <form onSubmit={loginUser}>

            <div className="form">
                <h2>Log in</h2>
                <div className="form-group">
                    <label htmlFor="email">E-mail</label>
                    <input id="email" name="email" type="email" onChange={(e) => setEmail(e.target.value)}></input>
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input id="password" name="password" type="password" onChange={(e) => setPassword(e.target.value)}></input>
                </div>
                { error ? <p style={{color: "red"}}>{error}</p> : null }
                <button id="log-in">Log in</button>
                <div>
                    <p>Not registered yet?</p>
                    <button onClick={() => setFormType('signup')}>Create Account</button>
                </div>
            </div>
        </form>            
    )
}

export default FormLogin