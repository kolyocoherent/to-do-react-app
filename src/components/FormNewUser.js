import React, {useContext, useState} from "react";
import axios from "axios";
import { UserLoggedInContext ,FormTypeContext } from "../Store";
import Cookies from 'universal-cookie';

function FormNewUser(props){
    const [userLogged, setUserLogged] = useContext(UserLoggedInContext);
    const [formType, setFormType] = useContext(FormTypeContext);
    const [name, setName] = useState('');
    const [age, setAge] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = React.useState(null);

    const cookies = new Cookies();


    const baseURL = "https://api-nodejs-todolist.herokuapp.com/user/register";

    function createPost() {
        axios
        .post(baseURL, {
            name: name,
            age: age,
            email: email,
            password: password,
            age: 20
        })
        .then((response) => {
            cookies.set('loggedIn', true, { path: '/' });
            setUserLogged("true")
        }).catch(error => {
            setError(error.response.data);
        });
    }
    const registerNewUser = e => {
        e.preventDefault();
        createPost()
    }
    return (
        <form onSubmit={registerNewUser}>           
            <div className="form">
                <h2>Create Account</h2>
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input id="name" name="name" type="text" onChange={(e) => setName(e.target.value)}></input>
                </div>
                <div className="form-group">
                    <label htmlFor="age">Current age</label>
                    <input id="age" name="age" type="number" onChange={(e) => setAge(e.target.value)}></input>
                </div>
                <div className="form-group">
                    <label htmlFor="email">E-mail</label>
                    <input id="email" name="email" type="email" onChange={(e) => setEmail(e.target.value)}></input>
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input id="password" name="password" type="password" onChange={(e) => setPassword(e.target.value)}></input>
                </div>
                    { error ? <p style={{color: "red"}}>{error}</p> : null }
                <input type="submit" value="Register"/>
                <div>
                    <p>Already have an account?</p>
                    <button onClick={() => setFormType('login')}>Log in</button>
                </div>

            </div>
        </form>
    )
}

export default FormNewUser