import React, {useContext, useEffect} from "react";
import FormLogin from "./FormLogin";
import FormNewUser from "./FormNewUser";
import Cookies from 'universal-cookie';
import { FormTypeContext } from "../Store";
import LogedIn from "./LoggedIn";


function LoginForm(){
    const cookies = new Cookies();
    const isLogged = cookies.get('loggedIn');

    const formContext = useContext(FormTypeContext)

    return (
        <div>
            { isLogged ? <LogedIn/>: formContext[0] == "login" ? <FormLogin/> : <FormNewUser/>}
        </div>
    )
}

export default LoginForm