import './App.css';
import React from "react";
import EnterSite from './components/EnterSite';
import Store from './Store';

const App = () => {

  return (
    <Store>
      <div className="App">
        <div className="flex site">
          <EnterSite/>
        </div>
      </div>
    </Store>
  );
}

export default App;
